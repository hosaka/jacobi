//
// Created by march on 23/03/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <time.h>

/*
* Plain C optimised implementation of the Jacobi algo
* For the best results, compile with:
*     gcc -O2 -lpthread -std=c11 -o plain plain.c
*/

/*
* Optimisation notes:
* 1. Utilising threading
* 2. Storing loop iterators in registers (not always a speed up)
* 3. Decreasing loop iterator reduces amount of instructions, rids of comparison: (i=max; i; i--)
* 4. Compiler level2 optimisation flag
* 5. std::memcpy is the fastest library routine for memory-to-memory copy
* 6. Mirrors and magnets!
*/

#define NODES 200
#define ITER 70000
#define HW_THREADS 4

/* objects with static storage duration will initialize to 0 if no initializer is specified */
static double matrix[NODES][NODES];
static double result[NODES];
static double new_guess[NODES];
double guess[NODES] = { [0 ... NODES-1] = 1.0};

/* thread arguments */
typedef struct{
    int start;
    int stop;
}ThreadArgs;

/*
* Jacobi algorithm, runs in threads
*/
void *jacobi_solve(void *args){
    ThreadArgs *arg = (ThreadArgs *)args;
    int start_node = arg -> start;
    int stop_node = arg -> stop;

    //printf("start at: %d, stop at: %d\n", start_node, stop_node);

    for (register int i = start_node; i < stop_node; i++) {
        double sum = 0.0;

        for (register int k = 0; k < NODES; k++) {
            if (i != k) {
                sum += matrix[i][k] * guess[k];
            }
        }
        new_guess[i] = (result[i] - sum) / matrix[i][i];
    }
}

/*
* Generate linear resistor network
*/
void gen_linear_net(){
    for(int row = 0; row < NODES; row++) {
        for(int col = 0; col < NODES; col++) {
            int d = abs(row - col);
            if(d == 0) matrix[row][col] = -2.0;
            if(d == 1) matrix[row][col] = 1.0;
            if(d > 1) matrix[row][col] = 0.0;
        }
    }
    result[0] = -1.0 * (double)(NODES + 1);
}

/*
* Divide chunks between nodes and spawn threads
*/
int main(int argc, char* argv[]) {

    /* threads structure and thread args */
    pthread_t workers[HW_THREADS];
    int chunk = NODES/HW_THREADS;

    int rc; // thread return, just assume they all work for now

    /* gen linear network */
    gen_linear_net();

    /* arguments per thread*/
    ThreadArgs worker_args[NODES];
    for (int t = 0; t < HW_THREADS; t++){
        worker_args[t].start = chunk * t;
        worker_args[t].stop = chunk * (t + 1);
    }

    printf("generating...\n");
    time_t t_start, t_stop;
    time(&t_start);

    /* iterate jacobi solver */
    for (register int iter = ITER; iter; iter--) {

        /* TODO: don't create new threads, resume/suspend them instead */
        /* create threads */
        register int t;
        for (t = 0; t < HW_THREADS; t++){
            rc = pthread_create(&workers[t], NULL, jacobi_solve, (void*)&worker_args[t]);
        }

        /* wait for threads to finish */
        for (t = 0; t < HW_THREADS; t++){
            rc = pthread_join(workers[t], NULL);
        }

        /* copy the guesses for the next thread iteration */
        memcpy(guess, new_guess, NODES * sizeof(double));

        /* TODO: stop iterating when the answer is reached, i.e all node from prev guess repeated in new one */
    }
    time(&t_stop);

    /* print guesses */
    for (int i = 0; i<NODES; i++){
        printf("    v%d:%12.5f", i, guess[i]);
        if (i % 4 == 0) printf("\n");
    }

    /* print timer */
    printf("\ntook: %d seconds\n", t_stop-t_start);

    /* kthxbye */
    return EXIT_SUCCESS;
}