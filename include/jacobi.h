//
// Created by march on 18/03/15.
//

#ifndef _JACOBI_JACOBI_H_
#define _JACOBI_JACOBI_H_

#include <thread>

using namespace std;

// jacobi thread class
class JacobiThread{
protected:
    // coefficient matrix and results vector
    double **matrix;
    double *result;
    double *guess;
    double *new_guess;

    // nodes in the resistor network
    int nodes;
    int start_node;
    int stop_node;
public:
    JacobiThread(double **, double *, double*, double*, int, int, int);

    void run();
    double *get_result();
    void join();

    virtual ~JacobiThread();
private:
    thread* this_thread;
    void solve_network();
};

#endif //_JACOBI_JACOBI_H_
