//
// Created by march on 18/03/15.
//

#ifndef _JACOBI_MAIN_H_
#define _JACOBI_MAIN_H_

#include <iostream>

void print_timer(time_t);
void help_exit();

// matrix generation
void gen_rand_matrix(double**, double*, int);
void gen_linear_matrix(double**, double*, int);

#endif //_JACOBI_MAIN_H_
