## Jacobi algorithm program to solve resistor networks

### Step 1:
 - Input/Create data for resistor network to obtain a matrix of coefficients and a vector of target values
   This can be hardwired, computed or input from a file
 - Implement the Jacobi algorithm as a single process. Confirm the correctness of returned output, add
   convergence detection and confirm effectiveness
 - CLI is acceptable

### Step 2:
 - Local parallel implementation
 - Using threads and messaging to make optimal use of multiple CPU cores on a single machine

### Step 3:
 - Distributed implementation
 - Using MPI on the HPC cluster
 - Single process initially, then with matrix rows distributed between several processors

### Optimisation:
 - Code optimisation
 - Consideration of number of processes and CPUs
 - Message passing to optimise speedup
 - Devising a sparse data structure to store matrix data (performance and memory improvements)