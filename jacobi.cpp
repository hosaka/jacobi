//
// Created by march on 18/03/15.
//

#include <iostream>
#include <thread>

#include "include/jacobi.h"

using namespace std;

JacobiThread::JacobiThread(double **matrix, double *result, double* guess,
        double *new_guess, int nodes, int start_node, int stop_node)
{
    // construct
    this->matrix = matrix;
    this->result = result;
    this->guess = guess;
    this->new_guess = new_guess;

    this->nodes = nodes;
    this->start_node = start_node;
    this->stop_node = stop_node;

    this_thread = nullptr;
}

void JacobiThread::solve_network() {
    // the jacobi algorithm, next round of guesses
    for (register int i = start_node; i < stop_node; ++i) {
        double sum = 0.0;

        for (register int k = 0; k < nodes; ++k) {
            if (i != k) {
                sum += matrix[i][k] * guess[k];
            }
        }
        new_guess[i] = (result[i] - sum) / matrix[i][i];
    }
}

double *JacobiThread::get_result() {
    // start the thread if it hasn't been started yet
    if (this_thread == nullptr){
        run();
    }
    // this_thread->join();
    //return guess_result;
}

void JacobiThread::join(){
    // join the thread when finished with the task
    this_thread->join();
}

void JacobiThread::run() {
    // don't start a thread if already running
    if (this_thread == nullptr){
        this_thread = new thread(&JacobiThread::solve_network, this);
    }
}

JacobiThread::~JacobiThread(){
    // destructor
}
