#include <iostream>
#include <getopt.h>
#include <random>
#include <thread>
#include <iomanip>
#include <cstring>

// user headers
#include "include/main.h"
#include "include/jacobi.h"

using namespace std;

/*
 * NOTE: Gauss-Seidel is a potential improvement to the Jacobi method of solving matriies
 * This implementation uses C++11 standard functions and has to be supported by the compiler
 *
 * For the best results, compile with flags:
 *     -O2 -std=c++11 -pthread -o
 */

/*
* Optimisation notes:
* 1. Utilising threading
* 2. Storing loop iterators in registers (not always a speed up)
* 3. Decreasing loop iterator reduces amount of instructions, rids of comparison: (i=max; i; i--)
* 4. Compiler level2 optimisation flag
* 5. std::memcpy is the fastest (but not the most efficient) library routine for memory-to-memory copy
* 6. Mirrors and magnets!
*/

// NOTE: use #define for constants instead of argv to speed up the execution
#define NODES 200
#define ITER 70000
#define HW_THREADS 4

#if 0
void gen_rand_matrix(double **matrix, double *result, int nodes, double min_value, double max_value){
    // type of random number distribution
    uniform_real_distribution<double> dist(min_value, max_value);  //(min, max)

    // Mersenne Twister: good quality random number generator
    mt19937 rng;

    // initialize with non-deterministic seeds
    rng.seed(random_device{}());

    // generate the matrix
    for (int i = 0; i < nodes; ++i) {
        for (int j = 0; j < nodes; ++j) {
            // rand fill in array
            matrix[i][j] = (double)dist(rng);
        }
        // generate the results vector
        result[i] = (double)dist(rng);
    }
}
#endif

// linear resistor network pattern
void gen_linear_matrix(double **matrix, double *results, int nodes){
    for (int i = 0; i < nodes; ++i) {
        for (int j = 0; j < nodes; ++j) {
            int d = abs(i - j);
            if(d == 0) matrix[i][j] = -2.0;
            if(d == 1) matrix[i][j] = 1.0;
            if(d > 1)  matrix[i][j] = 0.0;
        }
    }
    results[0] = -1.0 * (double)(nodes + 1);
}
int main(int argc, char* argv[]) {
    // number of nodes and iterations of jacobi algo
    int nodes = 0, max_iter = 0;

    // handle command line arguments
    if (argc > 1){
        int c;
        while ((c = getopt(argc, argv, "n:i:")) != -1){
            switch (c){
                // number of nodes
                case 'n':
                    nodes = atoi(optarg);
                    break;
                case 'i':
                    max_iter = atoi(optarg);
                    break;
                default:
                    help_exit();
            }
        }
    }
    else {
        help_exit();
    }

    // the OS can run this many threads in true concurrency
    int max_procs = thread::hardware_concurrency();

    // worker threads
    JacobiThread *workers[max_procs];

    // each thread will have it's own portion of nodes to solve
    int chunk = nodes/max_procs;

    // matrix of coefficients, results vector and algorithm guesses
    double matrix[nodes][nodes] = {{0.0}};
    double result[nodes] = {0.0};
    double guess[nodes] = {1.0};
    double new_guess[nodes] = {0.0};

    // surrogate array pointer
    double *matrix_p[nodes];
    for (size_t i = 0; i < nodes; ++i){
        matrix_p[i] = matrix[i];
    }

    // generate matrix and results vector
    //gen_rand_matrix(matrix_p, result, nodes, -12.0, 12.0);
    gen_linear_matrix(matrix_p, result, nodes);

    cout << "generating..." << endl;
    time_t t_start, t_stop;
    time(&t_start);

    // perform max iterations
    for (register int iter = max_iter; iter; --iter) {
#ifdef _ITER_TRACE
        if (iter % 1000 == 0) {
            cout << "iter: " << iter << " ";
        }
#endif
        /* TODO: don't create new threads, resume/suspend them instead */
        // create threads before running
        for (int proc = 0; proc < max_procs; ++proc) {
            workers[proc] = new JacobiThread(matrix_p, result, guess, new_guess,
                    nodes, chunk * proc, chunk * (proc + 1));
            //      total   start from        finish at
            workers[proc] -> run();
        }

        // join threads when finished
        for (JacobiThread *t : workers) {
            t->join();
        }

        // copy the guesses for the next thread iteration
        memcpy(guess, new_guess, nodes * sizeof(double));

        /* TODO: stop iterating when the answer is reached,
         * all node from prev guess repeated in new one
         */
#ifdef _ITER_TRACE
        for (int i = 0; i<nodes; i++){
            if (iter % 1000 == 0 && i<6){
                cout << "  v" << i << ":" << setw(12) << showpoint <<
                        setprecision(7) << guess[i] << " ";
            }
        }
        if (iter % 1000 == 0) {
            cout << endl;
        }
#endif
    } /* of iterations loop */

    time(&t_stop);

    // print results
    cout << "node values after solving " << max_iter << " iterations: " << endl;
    for (int i = 0; i<nodes; i++){
        cout << "    v" << i << ":" << setw(12) << showpoint <<
                setprecision(7) << guess[i];
        if (i % 4 == 0) cout << endl;
    }
    cout << endl;

    // display timer
    print_timer(t_stop - t_start);
    return EXIT_SUCCESS;
}

// TODO: consider using chrono timers for better resolution
void print_timer(time_t sec){
    cout << "jacobi took: " << sec << " seconds" << endl;
}

void help_exit(){
    cerr << "usage: " << "jacobi" << endl <<
            "    -n N - number of nodes in the network"<< endl <<
            "    -i I - number of iterations to perform" << endl;
    exit(EXIT_FAILURE);
}